# translation of kuickshow.po to British English
# Copyright (C) 2003, 2004, 2005, 2006 Free Software Foundation, Inc.
#
# Malcolm Hunter <malcolm.hunter@gmx.co.uk>, 2003, 2005, 2006.
# Andrew Coles <andrew_coles@yahoo.co.uk>, 2004, 2005, 2009.
# SPDX-FileCopyrightText: 2020, 2024 Steve Allewell <steve.allewell@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: kuickshow\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-12-18 01:56+0000\n"
"PO-Revision-Date: 2024-10-18 18:50+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 24.08.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Dwayne Bailey"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "dwayne@translate.org.za"

#. i18n: ectx: property (text), widget (QLabel, lblAuthors)
#: src/aboutwidget.ui:32
#, kde-format
msgid "(authors)"
msgstr "(authors)"

#. i18n: ectx: property (text), widget (KuickUrlWidget, urlHomepage)
#: src/aboutwidget.ui:45
#, kde-format
msgid "(url)"
msgstr "(URL)"

#. i18n: ectx: property (text), widget (QLabel, lblCopyright)
#: src/aboutwidget.ui:55
#, kde-format
msgid "(copyright)"
msgstr "(copyright)"

#. i18n: ectx: property (windowTitle), widget (QWidget, DefaultsWidget)
#: src/defaultswidget.ui:14
#, kde-format
msgid "Modifications"
msgstr "Modifications"

#. i18n: ectx: property (text), widget (QCheckBox, cbEnableMods)
#: src/defaultswidget.ui:23
#, kde-format
msgid "Apply default image modifications"
msgstr "Apply default image modifications"

#. i18n: ectx: property (title), widget (QGroupBox, gbScale)
#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: src/defaultswidget.ui:30 src/printing_page.ui:37
#, kde-format
msgid "Scaling"
msgstr "Scaling"

#. i18n: ectx: property (text), widget (QCheckBox, cbUpScale)
#: src/defaultswidget.ui:36
#, kde-format
msgid "Scale image to screen size, if smaller, up to factor:"
msgstr "Scale image to screen size, if smaller, up to factor:"

#. i18n: ectx: property (text), widget (QCheckBox, cbDownScale)
#: src/defaultswidget.ui:53
#, kde-format
msgid "Shrink image to screen size, if larger"
msgstr "Shrink image to screen size, if larger"

#. i18n: ectx: property (title), widget (QGroupBox, gbGeometry)
#: src/defaultswidget.ui:63
#, kde-format
msgid "Geometry"
msgstr "Geometry"

#. i18n: ectx: property (text), widget (QCheckBox, cbFlipVertically)
#: src/defaultswidget.ui:69
#, kde-format
msgid "Flip vertically"
msgstr "Flip vertically"

#. i18n: ectx: property (text), widget (QCheckBox, cbFlipHorizontally)
#: src/defaultswidget.ui:76
#, kde-format
msgid "Flip horizontally"
msgstr "Flip horizontally"

#. i18n: ectx: property (text), widget (QLabel, lbRotate)
#: src/defaultswidget.ui:85
#, kde-format
msgid "Rotate image:"
msgstr "Rotate image:"

#. i18n: ectx: property (text), item, widget (QComboBox, comboRotate)
#: src/defaultswidget.ui:93
#, kde-format
msgid "0 Degrees"
msgstr "0 Degrees"

#. i18n: ectx: property (text), item, widget (QComboBox, comboRotate)
#: src/defaultswidget.ui:98
#, kde-format
msgid "90 Degrees"
msgstr "90 Degrees"

#. i18n: ectx: property (text), item, widget (QComboBox, comboRotate)
#: src/defaultswidget.ui:103
#, kde-format
msgid "180 Degrees"
msgstr "180 Degrees"

#. i18n: ectx: property (text), item, widget (QComboBox, comboRotate)
#: src/defaultswidget.ui:108
#, kde-format
msgid "270 Degrees"
msgstr "270 Degrees"

#. i18n: ectx: property (title), widget (QGroupBox, gbAdjust)
#: src/defaultswidget.ui:134
#, kde-format
msgid "Adjustments"
msgstr "Adjustments"

#. i18n: ectx: property (text), widget (QLabel, label)
#: src/defaultswidget.ui:140
#, kde-format
msgid "Brightness:"
msgstr "Brightness:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: src/defaultswidget.ui:183
#, kde-format
msgid "Contrast:"
msgstr "Contrast:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: src/defaultswidget.ui:226
#, kde-format
msgid "Gamma:"
msgstr "Gamma:"

#. i18n: ectx: property (title), widget (QGroupBox, gbPreview)
#: src/defaultswidget.ui:272
#, kde-format
msgid "Preview"
msgstr "Preview"

#. i18n: ectx: property (text), widget (QLabel, lbImOrig)
#: src/defaultswidget.ui:281
#, kde-format
msgid "Original"
msgstr "Original"

#. i18n: ectx: property (text), widget (QLabel, lbImFiltered)
#: src/defaultswidget.ui:288
#, kde-format
msgid "Modified"
msgstr "Modified"

#. i18n: ectx: property (windowTitle), widget (QWidget, GeneralWidget)
#: src/generalwidget.ui:14
#, kde-format
msgid "General"
msgstr "General"

#. i18n: ectx: property (text), widget (QCheckBox, cbLastdir)
#: src/generalwidget.ui:20
#, kde-format
msgid "Remember last folder"
msgstr "Remember last folder"

#. i18n: ectx: property (title), widget (QGroupBox, gbox2)
#: src/generalwidget.ui:30
#, kde-format
msgid "Quality/Speed"
msgstr "Quality/Speed"

#. i18n: ectx: property (text), widget (QCheckBox, cbSmoothScale)
#: src/generalwidget.ui:36
#, kde-format
msgid "Smooth scaling"
msgstr "Smooth scaling"

#. i18n: ectx: property (text), widget (QCheckBox, cbFastRender)
#: src/generalwidget.ui:43
#, kde-format
msgid "Fast rendering"
msgstr "Fast rendering"

#. i18n: ectx: property (text), widget (QCheckBox, cbDither16bit)
#: src/generalwidget.ui:50
#, kde-format
msgid "Dither in HiColor (15/16bit) modes"
msgstr "Dither in HiColour (15/16bit) modes"

#. i18n: ectx: property (text), widget (QCheckBox, cbDither8bit)
#: src/generalwidget.ui:57
#, kde-format
msgid "Dither in LowColor (<=8bit) modes"
msgstr "Dither in LowColour (<=8bit) modes"

#. i18n: ectx: property (text), widget (QCheckBox, cbOwnPalette)
#: src/generalwidget.ui:64
#, kde-format
msgid "Use own color palette"
msgstr "Use own colour palette"

#. i18n: ectx: property (text), widget (QCheckBox, cbFastRemap)
#: src/generalwidget.ui:71
#, kde-format
msgid "Fast palette remapping"
msgstr "Fast palette remapping"

#. i18n: ectx: property (text), widget (QLabel, label)
#: src/generalwidget.ui:78
#, kde-format
msgid "Maximum cache size: "
msgstr "Maximum cache size: "

#. i18n: ectx: property (specialValueText), widget (QSpinBox, maxCacheSpinBox)
#: src/generalwidget.ui:88
#, kde-format
msgid "Unlimited"
msgstr "Unlimited"

#. i18n: ectx: property (suffix), widget (QSpinBox, maxCacheSpinBox)
#: src/generalwidget.ui:91
#, kde-format
msgid " MB"
msgstr " MB"

#. i18n: ectx: property (text), widget (QCheckBox, cbFullscreen)
#: src/generalwidget.ui:104
#, kde-format
msgid "Fullscreen mode"
msgstr "Fullscreen mode"

#. i18n: ectx: property (text), widget (QLabel, l0)
#: src/generalwidget.ui:114
#, kde-format
msgid "Background color:"
msgstr "Background colour:"

#. i18n: ectx: property (text), widget (QLabel, l1)
#: src/generalwidget.ui:121
#, kde-format
msgid "Show only files with extension: "
msgstr "Show only files with extension: "

#. i18n: ectx: property (tipText), widget (KUrlLabel, logo)
#: src/generalwidget.ui:128
#, kde-format
msgid "Open KuickShow Website"
msgstr "Open KuickShow Website"

#. i18n: ectx: property (text), widget (QCheckBox, cbPreload)
#: src/generalwidget.ui:138
#, kde-format
msgid "Preload next image"
msgstr "Preload next image"

#: src/imagewindow.cpp:138
#, kde-format
msgid "Duplicate Window"
msgstr "Duplicate Window"

#: src/imagewindow.cpp:143
#, kde-format
msgid "Show Next Image"
msgstr "Show Next Image"

#: src/imagewindow.cpp:148
#, kde-format
msgid "Show Previous Image"
msgstr "Show Previous Image"

#: src/imagewindow.cpp:153
#, kde-format
msgid "Delete Image"
msgstr "Delete Image"

#: src/imagewindow.cpp:158
#, kde-format
msgid "Move Image to Trash"
msgstr "Move Image to Wastebin"

#: src/imagewindow.cpp:171
#, kde-format
msgid "Restore Original Size"
msgstr "Restore Original Size"

#: src/imagewindow.cpp:176
#, kde-format
msgid "Maximize"
msgstr "Maximise"

#: src/imagewindow.cpp:181
#, kde-format
msgid "Rotate 90 Degrees"
msgstr "Rotate 90 Degrees"

#: src/imagewindow.cpp:186
#, kde-format
msgid "Rotate 180 Degrees"
msgstr "Rotate 180 Degrees"

#: src/imagewindow.cpp:191
#, kde-format
msgid "Rotate 270 Degrees"
msgstr "Rotate 270 Degrees"

#: src/imagewindow.cpp:196
#, kde-format
msgid "Flip Horizontally"
msgstr "Flip Horizontally"

#: src/imagewindow.cpp:201
#, kde-format
msgid "Flip Vertically"
msgstr "Flip Vertically"

#: src/imagewindow.cpp:206 src/kuickshow.cpp:1377
#, kde-format
msgid "Print Image..."
msgstr "Print Image..."

#: src/imagewindow.cpp:217
#, kde-format
msgid "Return to File Browser"
msgstr "Return to File Browser"

#: src/imagewindow.cpp:223
#, kde-format
msgid "More Brightness"
msgstr "More Brightness"

#: src/imagewindow.cpp:228
#, kde-format
msgid "Less Brightness"
msgstr "Less Brightness"

#: src/imagewindow.cpp:233
#, kde-format
msgid "More Contrast"
msgstr "More Contrast"

#: src/imagewindow.cpp:238
#, kde-format
msgid "Less Contrast"
msgstr "Less Contrast"

#: src/imagewindow.cpp:243
#, kde-format
msgid "More Gamma"
msgstr "More Gamma"

#: src/imagewindow.cpp:248
#, kde-format
msgid "Less Gamma"
msgstr "Less Gamma"

#: src/imagewindow.cpp:254
#, kde-format
msgid "Scroll Up"
msgstr "Scroll Up"

#: src/imagewindow.cpp:259
#, kde-format
msgid "Scroll Down"
msgstr "Scroll Down"

#: src/imagewindow.cpp:264
#, kde-format
msgid "Scroll Left"
msgstr "Scroll Left"

#: src/imagewindow.cpp:269
#, kde-format
msgid "Scroll Right"
msgstr "Scroll Right"

#: src/imagewindow.cpp:274
#, kde-format
msgid "Pause Slideshow"
msgstr "Pause Slideshow"

#: src/imagewindow.cpp:288
#, kde-format
msgid "Reload Image"
msgstr "Reload Image"

#: src/imagewindow.cpp:294
#, kde-format
msgid "Properties"
msgstr "Properties"

#: src/imagewindow.cpp:361
#, kde-format
msgctxt "Filename (Imagewidth x Imageheight)"
msgid "%3 (%1 x %2)"
msgstr "%3 (%1 x %2)"

#: src/imagewindow.cpp:426
#, kde-format
msgid "Unable to download the image from %1."
msgstr "Unable to download the image from %1."

#: src/imagewindow.cpp:442
#, kde-format
msgid ""
"Unable to load the image %1.\n"
"Perhaps the file format is unsupported or your Imlib is not installed "
"properly."
msgstr ""
"Unable to load the image %1.\n"
"Perhaps the file format is unsupported or your Imlib is not installed "
"properly."

#: src/imagewindow.cpp:851
#, kde-format
msgid "Brightness"
msgstr "Brightness"

#: src/imagewindow.cpp:855
#, kde-format
msgid "Contrast"
msgstr "Contrast"

#: src/imagewindow.cpp:859
#, kde-format
msgid "Gamma"
msgstr "Gamma"

#: src/imagewindow.cpp:906
#, kde-format
msgid "Unable to print the image."
msgstr "Unable to print the image."

#: src/imagewindow.cpp:907
#, kde-format
msgid "Printing Failed"
msgstr "Printing Failed"

#: src/imagewindow.cpp:924
#, kde-format
msgid "Keep original image size"
msgstr "Keep original image size"

#: src/imagewindow.cpp:929
#, kde-format
msgid "Save Image As"
msgstr "Save Image As"

#: src/imagewindow.cpp:934 src/kuickshow.cpp:1275
#, kde-format
msgid "Image Files (%1)"
msgstr "Image Files (%1)"

#: src/imagewindow.cpp:952
#, kde-format
msgid ""
"Could not save the file.\n"
"Perhaps the disk is full, or you do not have write permission to the file."
msgstr ""
"Could not save the file.\n"
"Perhaps the disk is full, or you do not have write permission to the file."

#: src/imagewindow.cpp:955
#, kde-format
msgid "File Saving Failed"
msgstr "File Saving Failed"

#: src/imagewindow.cpp:1202
#, kde-format
msgid ""
"You are about to view a very large image (%1 x %2 pixels), which can be very "
"resource-consuming and even make your computer hang.\n"
"Do you want to continue?"
msgstr ""
"You are about to view a very large image (%1 x %2 pixels), which can be very "
"resource-consuming and even make your computer hang.\n"
"Do you want to continue?"

#: src/imlib_imlib1.cpp:157
#, kde-format
msgid "Image loading and rendering library"
msgstr "Image loading and rendering library"

#: src/imlib_imlib2.cpp:119
#, kde-format
msgid "Image processing library"
msgstr "Image processing library"

#: src/kuickconfigdlg.cpp:40
#, kde-format
msgid "Configure"
msgstr "Configure"

#: src/kuickconfigdlg.cpp:46
#, kde-format
msgid "&General"
msgstr "&General"

#: src/kuickconfigdlg.cpp:50
#, kde-format
msgid "&Modifications"
msgstr "&Modifications"

#: src/kuickconfigdlg.cpp:54
#, kde-format
msgid "&Slideshow"
msgstr "&Slideshow"

#: src/kuickconfigdlg.cpp:60
#, kde-format
msgid "&Viewer Shortcuts"
msgstr "&Viewer Shortcuts"

#: src/kuickconfigdlg.cpp:63
#, kde-format
msgid "Bro&wser Shortcuts"
msgstr "Bro&wser Shortcuts"

#: src/kuickfile.cpp:128
#, kde-format
msgid "Downloading %1..."
msgstr "Downloading %1..."

#: src/kuickfile.cpp:129
#, kde-format
msgid ""
"Please wait while downloading\n"
"%1"
msgstr ""
"Please wait while downloading\n"
"%1"

#: src/kuickshow.cpp:171
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Do you really want to display this 1 image at the same time? This might be "
"quite resource intensive and could overload your computer.<br/>If you choose "
"<interface>%2</interface>, only the first image will be shown."
msgid_plural ""
"Do you really want to display these %1 images at the same time? This might "
"be quite resource intensive and could overload your computer.<br/>If you "
"choose <interface>%2</interface>, only the first image will be shown."
msgstr[0] ""
"Do you really want to display this 1 image at the same time? This might be "
"quite resource intensive and could overload your computer.<br/>If you choose "
"<interface>%2</interface>, only the first image will be shown."
msgstr[1] ""
"Do you really want to display these %1 images at the same time? This might "
"be quite resource intensive and could overload your computer.<br/>If you "
"choose <interface>%2</interface>, only the first image will be shown."

#: src/kuickshow.cpp:173
#, kde-format
msgid "Display Multiple Images?"
msgstr "Display Multiple Images?"

#: src/kuickshow.cpp:174
#, kde-format
msgid "Display"
msgstr "Display"

#: src/kuickshow.cpp:284
#, kde-format
msgid "&File"
msgstr "&File"

#: src/kuickshow.cpp:298
#, kde-format
msgid "&Edit"
msgstr "&Edit"

#: src/kuickshow.cpp:304
#, kde-format
msgid "View"
msgstr "View"

#: src/kuickshow.cpp:307
#, kde-format
msgid "&Settings"
msgstr "&Settings"

#: src/kuickshow.cpp:323
#, kde-format
msgid "Main Toolbar"
msgstr "Main Toolbar"

#: src/kuickshow.cpp:607
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Do you really want to permanently delete the file<nl/><filename>%1</"
"filename>?"
msgstr ""
"Do you really want to permanently delete the file<nl/><filename>%1</"
"filename>?"

#: src/kuickshow.cpp:608
#, kde-format
msgid "Delete File"
msgstr "Delete File"

#: src/kuickshow.cpp:633
#, kde-kuit-format
msgctxt "@info"
msgid "Do you really want to trash the file<nl/><filename>%1</filename>?"
msgstr "Do you really want to delete the file<nl/><filename>%1</filename>?"

#: src/kuickshow.cpp:634
#, kde-format
msgid "Trash File"
msgstr "Move File to Wastebin"

#: src/kuickshow.cpp:635
#, kde-format
msgctxt "to trash"
msgid "&Trash"
msgstr "Move &to Wastebin"

#: src/kuickshow.cpp:1150
#, kde-format
msgid "Image Error"
msgstr "Image Error"

#: src/kuickshow.cpp:1161
#, kde-format
msgid ""
"Unable to initialize the image library \"%2\".\n"
"\n"
"More information may be available in the session log file,\n"
"or can be obtained by starting %1 from a terminal."
msgstr ""
"Unable to initialise the image library \"%2\".\n"
"\n"
"More information may be available in the session log file,\n"
"or can be obtained by starting %1 from a terminal."

#: src/kuickshow.cpp:1165
#, kde-format
msgid "Fatal Initialization Error"
msgstr "Fatal Initialisation Error"

#: src/kuickshow.cpp:1274
#, kde-format
msgid "Select Files or Folder to Open"
msgstr "Select Files or Folder to Open"

#: src/kuickshow.cpp:1335
#, kde-format
msgid "Start Slideshow"
msgstr "Start Slideshow"

#: src/kuickshow.cpp:1338
#, kde-format
msgid "Show File Browser"
msgstr "Show File Browser"

#: src/kuickshow.cpp:1339
#, kde-format
msgid "Hide File Browser"
msgstr "Hide File Browser"

#: src/kuickshow.cpp:1343
#, kde-format
msgid "Configure %1..."
msgstr "Configure %1..."

#: src/kuickshow.cpp:1347
#, kde-format
msgid "About KuickShow"
msgstr "About KuickShow"

#: src/kuickshow.cpp:1354
#, kde-format
msgid "Open Only One Image Window"
msgstr "Open Only One Image Window"

#: src/kuickshow.cpp:1365
#, kde-format
msgid "Show Image"
msgstr "Show Image"

#: src/kuickshow.cpp:1369
#, kde-format
msgid "Show Image in Active Window"
msgstr "Show Image in Active Window"

#: src/kuickshow.cpp:1373
#, kde-format
msgid "Show Image in Fullscreen Mode"
msgstr "Show Image in Fullscreen Mode"

#: src/main.cpp:37
#, kde-format
msgid "KuickShow"
msgstr "KuickShow"

#: src/main.cpp:39
#, kde-format
msgid "A fast and versatile image viewer"
msgstr "A fast and versatile image viewer"

#: src/main.cpp:41
#, kde-format
msgid "(c) 1998-2009, Carsten Pfeiffer"
msgstr "(c) 1998-2009, Carsten Pfeiffer"

#: src/main.cpp:46
#, kde-format
msgid "Carsten Pfeiffer"
msgstr "Carsten Pfeiffer"

#: src/main.cpp:48
#, kde-format
msgid "Rober Hamberger"
msgstr "Rober Hamberger"

#: src/main.cpp:49
#, kde-format
msgid "Thorsten Scheuermann"
msgstr "Thorsten Scheuermann"

#: src/main.cpp:59
#, kde-format
msgid "Start in the last visited folder, not the current working folder."
msgstr "Start in the last visited folder, not the current working folder."

#: src/main.cpp:60
#, kde-format
msgid "Optional image filenames/urls to show"
msgstr "Optional image filenames/URLs to show"

#: src/printing.cpp:52
#, kde-format
msgid "Print %1"
msgstr "Print %1"

#. i18n: ectx: property (windowTitle), widget (QWidget, KuickPrintDialogPage)
#: src/printing_page.ui:14
#, kde-format
msgid "Image Settings"
msgstr "Image Settings"

#. i18n: ectx: property (text), widget (QCheckBox, addFileName)
#: src/printing_page.ui:20
#, kde-format
msgid "Print fi&lename below image"
msgstr "Print fi&lename below image"

#. i18n: ectx: property (text), widget (QCheckBox, blackwhite)
#: src/printing_page.ui:30
#, kde-format
msgid "Print image in &black and white"
msgstr "Print image in &black and white"

#. i18n: ectx: property (text), widget (QLabel, label)
#: src/printing_page.ui:53
#, kde-format
msgid "&Width:"
msgstr "&Width:"

#. i18n: ectx: property (text), item, widget (QComboBox, units)
#: src/printing_page.ui:64
#, kde-format
msgid "Millimeters"
msgstr "Millimetres"

#. i18n: ectx: property (text), item, widget (QComboBox, units)
#: src/printing_page.ui:69
#, kde-format
msgid "Centimeters"
msgstr "Centimetres"

#. i18n: ectx: property (text), item, widget (QComboBox, units)
#: src/printing_page.ui:74
#, kde-format
msgid "Inches"
msgstr "Inches"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: src/printing_page.ui:108
#, kde-format
msgid "&Height:"
msgstr "&Height:"

#. i18n: ectx: property (text), widget (QRadioButton, scale)
#: src/printing_page.ui:121
#, kde-format
msgid "Print e&xact size: "
msgstr "Print e&xact size: "

#. i18n: ectx: property (text), widget (QCheckBox, shrinkToFit)
#: src/printing_page.ui:141
#, kde-format
msgid "Shrink image to &fit, if necessary"
msgstr "Shrink image to &fit, if necessary"

#: src/slideshowwidget.cpp:46
#, kde-format
msgctxt "Run the slideshow until manually stopped"
msgid "Forever"
msgstr "Forever"

#. i18n: ectx: property (windowTitle), widget (QWidget, SlideShowWidget)
#: src/slideshowwidget.ui:14
#, kde-format
msgid "Slideshow"
msgstr "Slideshow"

#. i18n: ectx: property (text), widget (QCheckBox, fullScreen)
#: src/slideshowwidget.ui:20
#, kde-format
msgid "Switch to &full-screen"
msgstr "Switch to &full-screen"

#. i18n: ectx: property (text), widget (QCheckBox, startWithCurrent)
#: src/slideshowwidget.ui:27
#, kde-format
msgid "S&tart with current image"
msgstr "S&tart with current image"

#. i18n: ectx: property (text), widget (QLabel, label)
#: src/slideshowwidget.ui:34
#, kde-format
msgid "De&lay between slides:"
msgstr "De&lay between slides:"

#. i18n: ectx: property (specialValueText), widget (QSpinBox, delayTime)
#: src/slideshowwidget.ui:44
#, kde-format
msgid "Wait for key"
msgstr "Wait for key"

#. i18n: ectx: property (suffix), widget (QSpinBox, delayTime)
#: src/slideshowwidget.ui:47
#, kde-format
msgid " sec"
msgstr " sec"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: src/slideshowwidget.ui:57
#, kde-format
msgid "&Iterations:"
msgstr "&Iterations:"

#~ msgid ""
#~ "<qt>Do you really want to delete\n"
#~ " <b>'%1'</b>?</qt>"
#~ msgstr ""
#~ "<qt>Do you really want to delete\n"
#~ " <b>'%1'</b>?</qt>"

#~ msgid ""
#~ "Unable to initialize \"Imlib\".\n"
#~ "Start kuickshow from the command line and look for error messages.\n"
#~ "The program will now quit."
#~ msgstr ""
#~ "Unable to initialise \"Imlib\".\n"
#~ "Start kuickshow from the command line and look for error messages.\n"
#~ "The program will now quit."

#~ msgid "infinite"
#~ msgstr "infinite"

#~ msgid "Zoom In"
#~ msgstr "Zoom In"

#~ msgid "Zoom Out"
#~ msgstr "Zoom Out"
